package org.example;

import java.util.Arrays;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        int[] arr = new int[10];
        Random random = new Random();

        for(int i = 0; i < arr.length; i++) {
            while (true) {
               int rn = random.nextInt(10);
                if(arr[rn] == 0) {
                    arr[rn] = i + 1;
                    break;
                }
            }
        }
        System.out.println(Arrays.toString(arr));
    }
}




